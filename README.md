# Poker Hands
Author: jkasiraj


## Description
Poker Hands is an ASP .NET Core v2.2 WebApi written in C# with an Angular/JS frontend. It is used to evaluate two poker hands in a game and determine the winner.

The user may input two player names (should be unique) and 5 cards for each of the two hands. The expected format is two to three strings where the first 1-2 characters are one of (A, 2, 3, 4, 5, 6, 7, 8, 9, 10) for the card value and the last character is (D, S, C, H) for the suit value. Lowercase characters are currently not supported.


## Dependencies

* .Net Core 2.2
https://dotnet.microsoft.com/download/dotnet-core/2.2
* Angular CLI
https://cli.angular.io/
* npm
https://www.npmjs.com/get-npm

### Potential Dependencies (was not tested in other environments):
* OS: Windows 10 x64
* Browser: Chrome


## Usage -- Command Line

1) Navigate to folder that contains the package.json (i.e. poker-hands\package.json)
2) run command to install and run in debug mode:
        
        npm i
        npm run app


## Evaluating Poker Hands Algorithm
The current checks if the hand has a certain hand rank, starting from the highest (straight flush) and going to the lowest (high card). If it determines that a hand has a certain rank, it saves the high card that would differentiate that hand from other hands with similar ranks and returns. This algorithm was chosen for ease of code readability over efficiency, as that was not determined to be a priority for the user experience. An improved algorithm with would likely use a lookup table that involves encoding every special hand (that's not a high card) by hand (ignoring suit).

## Future improvements
* Prevent duplicate cards in players hands on front end side (for example: two ace of spaces in the same hand)
* Add option to configure the number of decks played with (assumption is currently 1 deck of 52 cards)
* Add containerization to improve deployment
* Add options and instructions to deply in production mode vs debug mode
* Add more user friendly error messages
* Make front end prettier, incoporating frameworks like Bootstrap (https://getbootstrap.com/)
* Use card suit to break ties further