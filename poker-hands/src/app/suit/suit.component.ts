import { Component, OnInit, Input } from '@angular/core';

@Component({
// tslint:disable-next-line: component-selector
  selector: '[app-suit]',
  templateUrl: './suit.component.html',
  styleUrls: ['./suit.component.less']
})
export class SuitComponent implements OnInit {
  @Input() suitKey: string;

  constructor() { }

  ngOnInit() {
  }

}
