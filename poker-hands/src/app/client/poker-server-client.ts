import axios from 'axios';
import { HandEvaluationRequest } from '../requests/hand-evaluation-request';

export class PokerServerClient {

    url = 'http://localhost:49733';

    evaluatePokerHands(body: HandEvaluationRequest, callback) {
        const hands = body.Hands;

        axios.post(this.url + '/api/poker', {
            hands,
          })
          .then( (response) => {
            if (response) {
                callback(null, response.data);
            } else {
                callback(null, response);
            }
          })
          .catch( (error) => {
            let errResponse = error;
            if (error && error.response && error.response.data) {
              errResponse = error.response.data;
            }
            callback(errResponse);
          });
    }
}
