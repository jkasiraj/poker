import { Component, OnInit, Input } from '@angular/core';
import { Card } from '../models/card';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {
  @Input() card: Card;
  @Input() isNewCard: boolean;
  cardStr: string;

  constructor() {
  }

  ngOnInit() {
    if (!this.card) { this.card = new Card(); }
  }

  setCard() {
    this.card.fromString(this.cardStr);
  }

}
