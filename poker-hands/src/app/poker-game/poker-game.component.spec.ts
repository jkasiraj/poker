import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PokerGameComponent } from './poker-game.component';
import { GameComponent } from '../game/game.component';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HandComponent } from '../hand/hand.component';
import { CardComponent } from '../card/card.component';
import { SuitComponent } from '../suit/suit.component';

describe('PokerGameComponent', () => {
  let component: PokerGameComponent;
  let fixture: ComponentFixture<PokerGameComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        FormsModule
      ],
      declarations: [
        PokerGameComponent,
        GameComponent,
        HandComponent,
        CardComponent,
        SuitComponent
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PokerGameComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
