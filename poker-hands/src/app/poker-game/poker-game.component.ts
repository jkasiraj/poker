import { Component, OnInit } from '@angular/core';
import { Game } from '../models/game';
import { Card } from '../models/card';
import { CardValue } from '../models/card-value';
import { Suit, SuitName } from '../models/suit';
import { Hand } from '../models/hand';

@Component({
  selector: 'app-poker-game',
  templateUrl: './poker-game.component.html',
  styleUrls: ['./poker-game.component.less']
})
export class PokerGameComponent implements OnInit {
  private myGame: Game;

  constructor() {
  }

  ngOnInit() {
    this.myGame = new Game();

    for (let i = 0; i < 2; i++) {
      const cards: Card[] = [];
      for (let j = 0; j < 5; j++) {
        const card = new Card();
        card.cardValue = this.getRandomCardValue();
        card.suit = this.getRandomSuit();
        cards.push(card);
      }
      this.myGame.addHand(new Hand(cards, 'Player ' + (i + 1)));
    }
  }

  getRandomCardValue(): CardValue {
    const cardValues = Object.values(CardValue);
    const len = cardValues.length - 1;
    const key = Math.floor(Math.random() * len) + 1;
    return cardValues[key];
  }

  getRandomSuit(): Suit {
    const suitNameValues = Object.values(SuitName);
    const len = suitNameValues.length - 1;
    const key = Math.floor(Math.random() * len) + 1;
    const suitName = suitNameValues[key];
    return new Suit(suitName);
  }
}
