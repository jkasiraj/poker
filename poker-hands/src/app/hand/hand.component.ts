import { Component, OnInit, Input } from '@angular/core';
import { Hand } from '../models/hand';

@Component({
  selector: 'app-hand',
  templateUrl: './hand.component.html',
  styleUrls: ['./hand.component.less']
})
export class HandComponent implements OnInit {
  @Input() hand: Hand;
  @Input() isNewHand: boolean;

  constructor() { }

  ngOnInit() {
    if (!this.hand) {this.hand = new Hand(); }
  }

}
