import { Component, OnInit, Input} from '@angular/core';
import { Game } from '../models/game';
import { HandEvaluationRequest } from '../requests/hand-evaluation-request';
import { PokerServerClient } from '../client/poker-server-client';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.less']
})
export class GameComponent implements OnInit {
  @Input() game: Game;
  @Input() isNewGame: boolean;
  private client: PokerServerClient;
  private msg: string;
  private errorMsg: string;
  private resetReady: boolean;

  constructor() { }

  ngOnInit() {
    this.client = new PokerServerClient();
    if (!this.game) { this.game = new Game(); }
    this.resetReady = true;
  }

  startGame() {
    this.updateMessage(null, 'Calculating results...');
    this.resetReady = false;
    this.isNewGame = false;
    const hands = this.game.hands;
    const req = new HandEvaluationRequest();
    req.Hands = [];
    for (const hand of hands) {
      req.Hands.push(hand.getHandInfo());
    }

    this.client.evaluatePokerHands(req, (err, res) => {
      if (res) { res += ' wins!!'; }
      this.updateMessage(err, res);
      this.resetReady = true;
    });
  }

  updateMessage(err, results) {
    if (err != null) {
      this.errorMsg = err;
      this.msg = null;
    } else if (results != null) {
      this.errorMsg = null;
      this.msg = results;
    } else {
       this.errorMsg = null;
       this.msg = null;
    }
  }

  resetGame() {
    this.updateMessage(null, null);
    this.isNewGame = true;
  }
}
