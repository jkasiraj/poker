export enum SuitName {
    Empty = '',
    Spade = 'Spade',
    Club = 'Club',
    Heart = 'Heart',
    Diamond = 'Diamond'
}

export enum SuitColor {
    Empty = '',
    Spade = 'black',
    Club = 'black',
    Heart = 'red',
    Diamond = 'red'
}

export class Suit {
    color: SuitColor;
    name: SuitName;

    constructor(suitName: SuitName = SuitName.Empty, suitColor: SuitColor = SuitColor.Empty) {
        this.initializeSuit(suitName, suitColor);
    }

    initializeSuit(suitName: SuitName, suitColor: SuitColor) {
        this.name = suitName;
        this.color = suitColor;
    }

    toString(): string {
        let results = '';
        switch (this.name) {
            case SuitName.Club:
                results += 'C';
                break;
            case SuitName.Diamond:
                results += 'D';
                break;
            case SuitName.Heart:
                results += 'H';
                break;
            case SuitName.Spade:
                results += 'S';
                break;
            default:
                break;
        }
        return results;
    }

    fromString(input: string) {
        switch (input) {
            case 'C':
                this.initializeSuit(SuitName.Club, SuitColor.Club);
                break;
            case 'D':
                this.initializeSuit(SuitName.Diamond, SuitColor.Diamond);
                break;
            case 'H':
                this.initializeSuit(SuitName.Heart, SuitColor.Heart);
                break;
            case 'S':
                this.initializeSuit(SuitName.Spade, SuitColor.Spade);
                break;
            default:
                break;
        }
    }
}
