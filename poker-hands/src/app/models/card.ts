import { CardValue } from './card-value';
import { Suit } from './suit';

export class Card {
    cardValue: CardValue;
    suit: Suit;

    constructor() {
        this.cardValue = CardValue.Empty;
        this.suit = new Suit();
    }

    toString(): string {
        let results = '';
        if (this.cardValue) { results += this.cardValue.valueOf(); }
        if (this.suit) { results += this.suit.toString(); }
        return results;
    }

    fromString(input: string): void {
        if (!input || input.length < 2 || input.length > 3) { return; }
        const newCardValue = this.parseCardValue(input.substring(0, input.length - 1));
        if (newCardValue !== CardValue.Empty) {this.cardValue = newCardValue; }
        this.suit.fromString(input[input.length - 1]);
    }

    parseCardValue(cardValStr: string) {
        switch (cardValStr) {
            case 'A':
                return CardValue.Ace;
            case 'J':
                return CardValue.Jack;
            case 'Q':
                return CardValue.Queen;
            case 'K':
                return CardValue.King;
            default:
                const num = this.parseNumber(cardValStr);
                if (num > 1 && num < 11) { return num.toString() as CardValue; }
                return CardValue.Empty;
        }
    }

    parseNumber(numberStr: string) {
        let result = 0;
        let digit = 1;
        for (let i = numberStr.length - 1; i >= 0; i--) {
            const curr = numberStr.charCodeAt(i) - '0'.charCodeAt(0);
            result += curr * digit;
            digit *= 10;
        }
        return result;
    }
}
