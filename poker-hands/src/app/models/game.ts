import { Hand } from './hand';

export class Game {
    public hands: Hand[];
    constructor() {
        this.hands = [];
    }

    addHand(newHand: Hand): boolean {
        if (newHand) {
            this.hands.push(newHand);
            return true;
        }
        return false;
    }
}
