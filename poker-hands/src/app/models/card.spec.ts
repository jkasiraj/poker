import { CardValue } from './card-value';
import { SuitName, SuitColor } from './suit';
import { Card } from './card';

describe('Card', () => {
    const fromCardStringTestCases = [
      { input: '10D',  cardValue: CardValue.Ten, suitName: SuitName.Diamond, suitColor: SuitColor.Diamond},
      { input: 'AH', cardValue: CardValue.Ace, suitName: SuitName.Heart, suitColor: SuitColor.Heart},
      { input: '2S', cardValue: CardValue.Two, suitName: SuitName.Spade, suitColor: SuitColor.Spade},
      { input: '3C', cardValue: CardValue.Three, suitName: SuitName.Club, suitColor: SuitColor.Club},
      { input: 'JD', cardValue: CardValue.Jack, suitName: SuitName.Diamond, suitColor: SuitColor.Diamond},
      { input: 'QH', cardValue: CardValue.Queen, suitName: SuitName.Heart, suitColor: SuitColor.Heart},
      { input: 'KS', cardValue: CardValue.King, suitName: SuitName.Spade, suitColor: SuitColor.Spade},
    ];

    fromCardStringTestCases.forEach( (testCase) => {
      const desc = 'should convert string to Card successfully for ' + testCase.input;
      it( desc, () => {
          const card = new Card();
          card.fromString(testCase.input);

          expect(card.suit.name).toBe(testCase.suitName);
          expect(card.suit.color).toBe(testCase.suitColor);
          expect(card.cardValue).toBe(testCase.cardValue);
          expect(testCase.input).toBe(card.toString());
      });
    });

    const fromCardStringInvalidTestCases = [
        { input: 'aad'},
        { input: 'ah'},
        { input: 'Ts'},
        { input: 'tc'},
        { input: 'jd'},
        { input: 'qh'},
        { input: 'ks'},
        { input: 'wD', suitName: SuitName.Diamond, suitColor: SuitColor.Diamond, resultsStr: 'D'},
      ];

    fromCardStringInvalidTestCases.forEach( (testCase) => {
      const desc = 'should not convert string to Card for invalid case ' + testCase.input;
      it( desc, () => {
        const card = new Card();
        card.fromString(testCase.input);

        let suitName = SuitName.Empty;
        let suitColor = SuitColor.Empty;
        let results = '';

        if (testCase.suitName) { suitName = testCase.suitName; }
        if (testCase.suitColor) { suitColor = testCase.suitColor; }
        if (testCase.resultsStr) { results = testCase.resultsStr; }

        expect(card.cardValue).toBe(CardValue.Empty);
        expect(card.suit.name).toBe(suitName);
        expect(card.suit.color).toBe(suitColor);
        expect(card.toString()).toBe(results);
      });
    });

    const fromNumberString = [
        { input: '2', expected: 2},
        { input: '3', expected: 3},
        { input: '4', expected: 4},
        { input: '5', expected: 5},
        { input: '6', expected: 6},
        { input: '7', expected: 7},
        { input: '8', expected: 8},
        { input: '9', expected: 9},
        { input: '10', expected: 10},
        { input: 'A', expected: 17},
      ];

    fromNumberString.forEach( (testCase) => {
      const desc = 'should convert string to number successfully ' + testCase.input;
      it( desc, () => {
        const card = new Card();
        expect(card.parseNumber(testCase.input)).toBe(testCase.expected);
      });
    });

  });
