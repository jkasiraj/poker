import { Card } from './card';
import { HandInfo } from './hand-info';

export class Hand {
    private maxCards: number;
    public cards: Card[];
    public owner: string;
    constructor(cards: Card[] = [], owner: string = '', maxCards: number = 5) {
        if (!cards) { cards = []; }
        this.maxCards = maxCards;
        this.cards = cards;
        this.owner = owner;
    }

    addCards(newCards: Card[]): boolean {
        if (newCards && this.cards.length + newCards.length <= this.maxCards) {
            newCards.forEach(newCard => this.cards.push(newCard));
            return true;
        }
        return false;
    }

    getHandInfo(): HandInfo {
        const handInfo = new HandInfo();
        handInfo.id = this.owner;

        for (const card of this.cards) {
            handInfo.cards.push(card.toString());
        }
        return handInfo;
    }
}
