import { HandInfo } from '../models/hand-info';

export class HandEvaluationRequest {
    Hands: HandInfo[];
}
