import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CardComponent } from './card/card.component';
import { HandComponent } from './hand/hand.component';
import { GameComponent } from './game/game.component';
import { SuitComponent } from './suit/suit.component';
import { PokerGameComponent } from './poker-game/poker-game.component';

@NgModule({
  declarations: [
    AppComponent,
    PokerGameComponent,
    GameComponent,
    HandComponent,
    CardComponent,
    SuitComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
