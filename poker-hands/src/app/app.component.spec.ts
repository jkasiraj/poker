import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { PokerGameComponent } from './poker-game/poker-game.component';
import { BrowserModule } from '@angular/platform-browser';
import { GameComponent } from './game/game.component';
import { CardComponent } from './card/card.component';
import { HandComponent } from './hand/hand.component';
import { SuitComponent } from './suit/suit.component';
import { FormsModule } from '@angular/forms';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        BrowserModule,
        FormsModule,
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        PokerGameComponent,
        GameComponent,
        HandComponent,
        CardComponent,
        SuitComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });
});
