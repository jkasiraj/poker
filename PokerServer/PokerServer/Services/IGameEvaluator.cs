﻿using System.Collections.Generic;
using PokerServer.Models;

namespace PokerServer.Services
{
    public interface IGameEvaluator
    {
        bool IsValidGame(List<HandInfo> hands, int numDecks);
        string GetBestHand(List<HandInfo> hands);
    }
}
