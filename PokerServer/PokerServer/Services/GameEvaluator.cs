﻿using System.Collections.Generic;
using System.Linq;
using PokerServer.Models;
using PokerServer.Models.Enums;

namespace PokerServer.Services
{
    public class GameEvaluator : IGameEvaluator
    {
        private readonly IHandEvaluator _handEvaluator;

        public GameEvaluator(IHandEvaluator handEvaluator = null)
        {
            _handEvaluator = handEvaluator ?? new HandEvaluator();
        }

        public bool IsValidGame(List<HandInfo> hands, int numDecks)
        {
            var idDict = new HashSet<string>();
            var cardDict = new Dictionary<string, int>();

            foreach (var hand in hands)
            {
                if (idDict.Contains(hand.Id)) return false;
                idDict.Add(hand.Id);
                foreach (var card in hand.Cards)
                {
                    if (cardDict.ContainsKey(card))
                    {
                        if (cardDict[card] == numDecks) return false;
                        cardDict[card] += 1;
                    }
                    else
                    {
                        cardDict.Add(card, 1);
                    }
                }
            }
            return true;
        }

        public string GetBestHand(List<HandInfo> hands)
        {
            var handValues = _handEvaluator.GetHandValues(hands);

            handValues.Sort((h1, h2) => h2.HandRank.CompareTo(h1.HandRank));

            var possibilities = handValues
                .Where(h => h.HandRank == handValues?.FirstOrDefault()?.HandRank).ToList();

            var id = possibilities.FirstOrDefault()?.Id;
            if (possibilities.Count > 1)
            {
                if (possibilities[0].HighCard.CardValue == possibilities[1].HighCard.CardValue)
                {
                    id = "It's a tie! Nobody (everybody?) ";
                }
                else
                {
                    possibilities.Sort((h1, h2) => h2.HighCard.CardValue.CompareTo(h1.HighCard.CardValue));
                    id = possibilities.FirstOrDefault()?.Id;
                }
            }

            return id;
        }
    }
}
