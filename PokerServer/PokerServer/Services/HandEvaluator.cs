﻿using System;
using System.Collections.Generic;
using System.Linq;
using PokerServer.Models;
using PokerServer.Models.Enums;

namespace PokerServer.Services
{
    public class HandEvaluator : IHandEvaluator
    {
        public List<HandValue> GetHandValues(List<HandInfo> hands)
        {
            var handValues = new List<HandValue>();
            foreach (var hand in hands)
            {
                handValues.Add(GetHandValue(hand));
            }
            return handValues;
        }

        public HandValue GetHandValue(HandInfo hand)
        {
            if (hand?.Cards == null || hand.Cards.Count != 5) throw new ArgumentException(nameof(hand));
            var id = hand.Id;
            var cards = new List<Card>();
            foreach (var c in hand.Cards)
            {
                cards.Add(new Card(c));
            }
            cards.Sort((c1, c2) => c1.CardValue.ToString().CompareTo(c2.CardValue.ToString()));
            return GetHandValue(id, cards);
        }

        protected HandValue GetHandValue(string id, List<Card> cards)
        {
            var results = new HandValue()
            {
                Id = id
            };
            Card highCard = null;

            highCard = IsStraightFlush(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.StraightFlush;
                return results;
            }
            highCard = IsFourOfAKind(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.FourOfAKind;
                return results;
            }
            highCard = IsFullHouse(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.FullHouse;
                return results;
            }
            highCard = IsFlush(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.Flush;
                return results;
            }
            highCard = IsStraight(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.Straight;
                return results;
            }
            highCard = IsLowAceStraight(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.Straight;
                return results;
            }
            highCard = IsThreeOfAKind(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.ThreeOfAKind;
                return results;
            }
            highCard = IsTwoPair(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.TwoPair;
                return results;
            }
            highCard = IsPair(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.Pair;
                return results;
            }
            highCard = IsHighCard(cards);
            if (highCard != null)
            {
                results.HighCard = highCard;
                results.HandRank = HandRank.HighCard;
                return results;
            }
            return results;
        }

        protected Card IsStraightFlush(List<Card> sortedCards)
        {
            if (IsFlush(sortedCards) == null) return null;
            var results = IsStraight(sortedCards);
            if (results != null) return results;
            return IsLowAceStraight(sortedCards);
        }

        protected Card IsFourOfAKind(List<Card> sortedCards)
        {
            if (sortedCards[0].CardValue == sortedCards[sortedCards.Count - 2].CardValue) return sortedCards[0];
            if (sortedCards[1].CardValue == sortedCards[sortedCards.Count - 1].CardValue) return sortedCards[1];

            return null;
        }
        protected Card IsFullHouse(List<Card> sortedCards)
        {
            if (sortedCards[0].CardValue == sortedCards[2].CardValue && sortedCards[3].CardValue == sortedCards[4].CardValue) return sortedCards[0];
            if (sortedCards[0].CardValue == sortedCards[1].CardValue && sortedCards[2].CardValue == sortedCards[4].CardValue) return sortedCards[2];

            return null;
        }

        protected Card IsFlush(List<Card> sortedCards)
        {
            if (sortedCards.GroupBy((c) => c.Suit).Any(g => g.Count() < 5)) return null;
            return sortedCards[0];
        }

        protected Card IsStraight(List<Card> sortedCards)
        {
            for (int i = 1; i < sortedCards.Count - 1; i++)
            {
                if (sortedCards[i].CardValue - sortedCards[i + 1].CardValue != 1) return null;
            }

            if (sortedCards[0].CardValue - sortedCards[1].CardValue == 1) return sortedCards[0];
            if (sortedCards[sortedCards.Count - 2].CardValue - sortedCards[sortedCards.Count - 1].CardValue == 1) return sortedCards[1];

            return null;
        }

        protected Card IsLowAceStraight(List<Card> sortedCards)
        {

            bool checkLowAce = false;
            var lowAceList = new List<Card>();
            for (int i = 0; i < sortedCards.Count; i++)
            {
                if (sortedCards[i].CardValue == CardValue.Ace)
                {
                    var lowAceCard = new Card(CardValue.AceLow, sortedCards[i].Suit)
                    {

                    };
                    lowAceList.Add(lowAceCard);
                    checkLowAce = true;
                }
                else lowAceList.Add(sortedCards[i]);
            }

            if (checkLowAce)
            {
                lowAceList.Sort((c1, c2) => c1.CardValue.ToString().CompareTo(c2.CardValue.ToString()));
                return IsStraight(lowAceList);
            }

            return null;
        }

        protected Card IsThreeOfAKind(List<Card> sortedCards)
        {
            for (int i = 0; i < 3; i++)
            {
                if (sortedCards[i].CardValue == sortedCards[i + 2].CardValue) return sortedCards[i];
            }

            return null;
        }

        protected Card IsTwoPair(List<Card> sortedCards)
        {
            for (int i = 0; i < 2; i++)
            {
                if (sortedCards[i].CardValue == sortedCards[i + 1].CardValue && sortedCards[i + 2].CardValue == sortedCards[i + 3].CardValue) return sortedCards[i];
            }
            return null;
        }

        protected Card IsPair(List<Card> sortedCards)
        {
            for (int i = 0; i < 4; i++)
            {
                if (sortedCards[i].CardValue == sortedCards[i + 1].CardValue) return sortedCards[i];
            }
            return null;
        }

        protected Card IsHighCard(List<Card> sortedCards)
        {
            return sortedCards[0];
        }
    }
}
