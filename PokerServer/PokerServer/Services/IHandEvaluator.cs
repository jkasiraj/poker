﻿using System.Collections.Generic;
using PokerServer.Models;
using PokerServer.Models.Enums;

namespace PokerServer.Services
{
    public interface IHandEvaluator
    {
        HandValue GetHandValue(HandInfo hand);

        List<HandValue> GetHandValues(List<HandInfo> hands);

    }
}
