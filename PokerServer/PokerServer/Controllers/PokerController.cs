﻿using System;
using Microsoft.AspNetCore.Mvc;
using PokerServer.Models.Requests;
using PokerServer.Services;

namespace PokerServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PokerController : ControllerBase
    {
        private readonly IGameEvaluator _gameEvaluator;

        public PokerController()
        {
            _gameEvaluator = new GameEvaluator();
        }

        // GET api/poker
        [HttpGet]
        public ActionResult<string> PokerHealth()
        {
            return Ok("PokerController is running!");
        }

        // POST api/poker
        [HttpPost]
        public ActionResult<string> EvaluatePokerHands([FromBody] EvaluateHandRequest request)
        {
            if (!ModelState.IsValid) return BadRequest("Invalid Request");

            try
            {
                if (!_gameEvaluator.IsValidGame(request.Hands, 1)) return BadRequest("Invalid game provided");
                var winner = _gameEvaluator.GetBestHand(request.Hands);
                return Ok(winner);
            }
            catch (Exception e)
            {
                return BadRequest(e.ToString());
            }
        }

    }
}
