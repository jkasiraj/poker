﻿using System.Collections.Generic;

namespace PokerServer.Models
{
    public class HandInfo
    {
        public string Id;

        public List<string> Cards;
    }
}
