﻿

namespace PokerServer.Models.Enums
{
    public enum Suit
    {
        Spade = 'A',
        Club = 'B',
        Diamond = 'C',
        Heart = 'D',
    }
}
