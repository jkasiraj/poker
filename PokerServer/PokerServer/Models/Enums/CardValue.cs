﻿

namespace PokerServer.Models.Enums
{
    public enum CardValue
    {
        AceLow = 'A',
        Two = 'B',
        Three = 'C',
        Four = 'D',
        Five = 'E',
        Six = 'F',
        Seven = 'G',
        Eight = 'H',
        Nine = 'I',
        Ten = 'J',
        Jack = 'K',
        Queen = 'L',
        King = 'M',
        Ace = 'N',
    }

}
