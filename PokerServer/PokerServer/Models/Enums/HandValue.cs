﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PokerServer.Models.Enums
{
    public class HandValue
    {
        public HandRank HandRank;
        public Card HighCard;
        public string Id;
    }
}
