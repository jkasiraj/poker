﻿using System.Collections.Generic;

namespace PokerServer.Models.Requests
{
    public class EvaluateHandRequest
    {
        public List<HandInfo> Hands;
    }
}
