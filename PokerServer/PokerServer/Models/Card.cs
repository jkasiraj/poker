﻿using System;
using PokerServer.Models.Enums;

namespace PokerServer.Models
{
    public class Card
    {
        public readonly CardValue CardValue;
        public readonly Suit Suit;
        private readonly string _cardStr;

        public Card(CardValue cardValue, Suit suit)
        {
            CardValue = cardValue;
            Suit = suit;
        }

        public Card(string cardStr)
        {
            _cardStr = cardStr ?? throw new ArgumentNullException(nameof(cardStr));
            if (_cardStr.Length < 2 || _cardStr.Length > 4) throw new ArgumentException(nameof(cardStr));
            Suit = ParseSuit(_cardStr[_cardStr.Length - 1]);
            CardValue = ParseCardValue(_cardStr.Substring(0, _cardStr.Length - 1));
        }

        public static CardValue ParseCardValue(string cardValStr)
        {
            switch (cardValStr)
            {
                case "A":
                    return CardValue.Ace;
                case "J":
                    return CardValue.Jack;
                case "Q":
                    return CardValue.Queen;
                case "K":
                    return CardValue.King;
                case "2":
                    return CardValue.Two;
                case "3":
                    return CardValue.Three;
                case "4":
                    return CardValue.Four;
                case "5":
                    return CardValue.Five;
                case "6":
                    return CardValue.Six;
                case "7":
                    return CardValue.Seven;
                case "8":
                    return CardValue.Eight;
                case "9":
                    return CardValue.Nine;
                case "10":
                    return CardValue.Ten;
                default:
                    throw new Exception("Invalid Card");
            }
        }

        public static Suit ParseSuit(char suitChar)
        {
            switch (suitChar)
            {
                case 'C':
                    return Suit.Club;
                case 'D':
                    return Suit.Diamond;
                case 'H':
                    return Suit.Heart;
                case 'S':
                    return Suit.Spade;
                default:
                    throw new ArgumentException(nameof(suitChar));
            }
        }
    }
}
