using System;
using PokerServer.Models;
using PokerServer.Models.Enums;
using Xunit;

namespace PokerServerTests.Models
{
    public class CardTests
    {
        [Fact]
        public void Cards_Constructor_InvalidInput()
        {
            // Act + Assert
            Assert.Throws<ArgumentNullException>(() => new Card(null));
            Assert.Throws<ArgumentException>(() => new Card(""));
            Assert.Throws<ArgumentException>(() => new Card("h"));
            Assert.Throws<ArgumentException>(() => new Card("erro"));
        }

        [Theory]
        [InlineData("A", (int)CardValue.Ace)]
        [InlineData("2", (int)CardValue.Two)]
        [InlineData("3", (int)CardValue.Three)]
        [InlineData("4", (int)CardValue.Four)]
        [InlineData("5", (int)CardValue.Five)]
        [InlineData("6", (int)CardValue.Six)]
        [InlineData("7", (int)CardValue.Seven)]
        [InlineData("8", (int)CardValue.Eight)]
        [InlineData("9", (int)CardValue.Nine)]
        [InlineData("10", (int)CardValue.Ten)]
        [InlineData("J", (int)CardValue.Jack)]
        [InlineData("Q", (int)CardValue.Queen)]
        [InlineData("K", (int)CardValue.King)]
        public void Cards_ParseCardValue_Success(string input, int expected)
        {
            // Act + Assert
            Assert.Equal(expected, (int) Card.ParseCardValue(input));
        }

        [Theory]
        [InlineData('C', (int) Suit.Club)]
        [InlineData('D', (int) Suit.Diamond)]
        [InlineData('H', (int) Suit.Heart)]
        [InlineData('S', (int) Suit.Spade)]
        public void Cards_ParseSuit_Success(char input, int expected)
        {
            // Act + Assert
            Assert.Equal(expected, (int) Card.ParseSuit(input));
        }
    }
}
