﻿using System.Collections.Generic;
using PokerServer.Models;
using PokerServer.Models.Enums;
using PokerServer.Services;
using Xunit;

namespace PokerServerTests.Services
{
    public class HandEvaluatorTests : HandEvaluator
    {

        [Fact]
        public void HandEvaluator_GetHandValue_CardString_StraightFlush()
        {
            // Arrange
            var id = "testing";
            var highCard = new Card(CardValue.Five, Suit.Spade);
            var hand = new List<string>()
            {
                "AS",
                "5S",
                "4S",
                "3S",
                "2S",
            };
            var handInfo = new HandInfo()
            {
                Cards = hand,
                Id = id,
            };

            // Act
            var results = this.GetHandValue(handInfo);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(highCard.CardValue, results.HighCard.CardValue);
            Assert.Equal(highCard.Suit, results.HighCard.Suit);
            Assert.Equal(id, results.Id);
            Assert.Equal(HandRank.StraightFlush, results.HandRank);
        }

        [Fact]
        public void HandEvaluator_GetHandValue_CardString_HighCard()
        {
            // Arrange
            var id = "testing";
            var highCard = new Card(CardValue.Ace, Suit.Club);
            var hand = new List<string>()
            {
                "AC",
                "6D",
                "5D",
                "3D",
                "2D",
            };
            var handInfo = new HandInfo()
            {
                Cards = hand,
                Id = id,
            };

            // Act
            var results = this.GetHandValue(handInfo);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(highCard.CardValue, results.HighCard.CardValue);
            Assert.Equal(highCard.Suit, results.HighCard.Suit);
            Assert.Equal(id, results.Id);
            Assert.Equal(HandRank.HighCard, results.HandRank);
        }

        [Fact]
        public void HandEvaluator_GetHandValue_CardList_StraightFlush()
        {
            // Arrange
            var id = "testing";
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.Five, Suit.Spade),
                new Card(CardValue.Four, Suit.Spade),
                new Card(CardValue.Three, Suit.Spade),
                new Card(CardValue.Two, Suit.Spade),
            };

            // Act
            var results = this.GetHandValue(id, hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[1].CardValue, results.HighCard.CardValue);
            Assert.Equal(hand[1].Suit, results.HighCard.Suit);
            Assert.Equal(id, results.Id);
            Assert.Equal(HandRank.StraightFlush, results.HandRank);
        }

        [Fact]
        public void HandEvaluator_GetHandValue_CardList_HighCard()
        {
            // Arrange
            var id = "testing";
            var hand = GetBadHand();

            // Act
            var results = this.GetHandValue(id, hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.HighCard.CardValue);
            Assert.Equal(hand[0].Suit, results.HighCard.Suit);
            Assert.Equal(id, results.Id);
            Assert.Equal(HandRank.HighCard, results.HandRank);
        }

        [Fact]
        public void HandEvaluator_StraightFlush_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.King, Suit.Spade),
                new Card(CardValue.Queen, Suit.Spade),
                new Card(CardValue.Jack, Suit.Spade),
                new Card(CardValue.Ten, Suit.Spade),
            };

            // Act
            var results = this.IsStraightFlush(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_StraightFlush2_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.Five, Suit.Spade),
                new Card(CardValue.Four, Suit.Spade),
                new Card(CardValue.Three, Suit.Spade),
                new Card(CardValue.Two, Suit.Spade),
            };

            // Act
            var results = this.IsStraightFlush(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[1].CardValue, results.CardValue);
            Assert.Equal(hand[1].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_StraightFlush_Fail()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.King, Suit.Spade),
                new Card(CardValue.Queen, Suit.Spade),
                new Card(CardValue.Jack, Suit.Spade),
                new Card(CardValue.Nine, Suit.Spade),
            };

            // Act
            var results = this.IsStraightFlush(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_FourOfAKind_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.Ace, Suit.Heart),
            };

            // Act
            var results = this.IsFourOfAKind(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }


        [Fact]
        public void HandEvaluator_FourOfAKind2_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Two, Suit.Club),
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.Ace, Suit.Heart),
            };

            // Act
            var results = this.IsFourOfAKind(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[1].CardValue, results.CardValue);
            Assert.Equal(hand[1].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_FourOfAKind_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsFourOfAKind(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_FullHouse_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.King, Suit.Spade),
                new Card(CardValue.King, Suit.Diamond),
                new Card(CardValue.King, Suit.Club),
                new Card(CardValue.Jack, Suit.Diamond),
                new Card(CardValue.Jack, Suit.Spade),
            };

            // Act
            var results = this.IsFullHouse(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }


        [Fact]
        public void HandEvaluator_FullHouse2_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.King, Suit.Spade),
                new Card(CardValue.King, Suit.Diamond),
                new Card(CardValue.Jack, Suit.Club),
                new Card(CardValue.Jack, Suit.Diamond),
                new Card(CardValue.Jack, Suit.Spade),
            };

            // Act
            var results = this.IsFullHouse(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[2].CardValue, results.CardValue);
            Assert.Equal(hand[2].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_FullHouse_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsFullHouse(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_Flush_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.King, Suit.Spade),
                new Card(CardValue.Queen, Suit.Spade),
                new Card(CardValue.Jack, Suit.Spade),
                new Card(CardValue.Ten, Suit.Spade),
            };

            // Act
            var results = this.IsFlush(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_Flush_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsFlush(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_IsStraight_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Spade),
                new Card(CardValue.King, Suit.Diamond),
                new Card(CardValue.Queen, Suit.Club),
                new Card(CardValue.Jack, Suit.Spade),
                new Card(CardValue.Ten, Suit.Spade),
            };

            // Act
            var results = this.IsStraight(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_Straight_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsStraight(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_LowAceStraight_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Four, Suit.Diamond),
                new Card(CardValue.Five, Suit.Club),
                new Card(CardValue.Three, Suit.Spade),
                new Card(CardValue.Two, Suit.Spade),
                new Card(CardValue.Ace, Suit.Spade),
            };

            // Act
            var results = this.IsLowAceStraight(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[1].CardValue, results.CardValue);
            Assert.Equal(hand[1].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_LowAceStraight_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsLowAceStraight(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_LowAceStraight_NoAce_Fail()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Two, Suit.Diamond),
                new Card(CardValue.Three, Suit.Diamond),
                new Card(CardValue.Four, Suit.Club),
                new Card(CardValue.Five, Suit.Diamond),
                new Card(CardValue.Six, Suit.Diamond),
            };

            // Act
            var results = this.IsLowAceStraight(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_ThreeOfAKind_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Spade),
            };

            // Act
            var results = this.IsThreeOfAKind(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_ThreeOfAKind2_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Two, Suit.Club),
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Spade),
            };

            // Act
            var results = this.IsThreeOfAKind(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[1].CardValue, results.CardValue);
            Assert.Equal(hand[1].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_ThreeOfAKind3_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Two, Suit.Club),
                new Card(CardValue.Two, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Ace, Suit.Spade),
            };

            // Act
            var results = this.IsThreeOfAKind(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[2].CardValue, results.CardValue);
            Assert.Equal(hand[2].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_ThreeOfAKind_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsThreeOfAKind(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_TwoPair_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
                new Card(CardValue.Two, Suit.Diamond),
                new Card(CardValue.Two, Suit.Diamond),
            };

            // Act
            var results = this.IsTwoPair(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_TwoPair_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsTwoPair(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_Pair_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Ace, Suit.Diamond),
            };

            // Act
            var results = this.IsPair(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        [Fact]
        public void HandEvaluator_Pair_Fail()
        {
            // Arrange
            var hand = GetBadHand();

            // Act
            var results = this.IsPair(hand);

            // Assert
            Assert.Null(results);
        }

        [Fact]
        public void HandEvaluator_HighCard_Pass()
        {
            // Arrange
            var hand = new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
            };

            // Act
            var results = this.IsHighCard(hand);

            // Assert
            Assert.NotNull(results);
            Assert.Equal(hand[0].CardValue, results.CardValue);
            Assert.Equal(hand[0].Suit, results.Suit);
        }

        private List<Card> GetBadHand()
        {
            return new List<Card>()
            {
                new Card(CardValue.Ace, Suit.Club),
                new Card(CardValue.Six, Suit.Diamond),
                new Card(CardValue.Five, Suit.Diamond),
                new Card(CardValue.Three, Suit.Diamond),
                new Card(CardValue.Two, Suit.Diamond),
            };

        }
    }
}
