﻿using System.Collections.Generic;
using Moq;
using PokerServer.Models;
using PokerServer.Models.Enums;
using PokerServer.Services;
using Xunit;

namespace PokerServerTests.Services
{
    public class GameEvaluatorTests
    {

        [Fact]
        public void GameEvaluator_GetBestHand_DifferentHandRanks_Passes()
        {
            // Arrange
            var id = "testing";
            var id2 = "testing2";
            List <HandValue> handValues = new List<HandValue>()
            {
                new HandValue()
                {
                    HandRank =  HandRank.Flush,
                    HighCard = new Card(CardValue.Ace, Suit.Club),
                    Id = id,
                },
                new HandValue()
                {
                    HandRank =  HandRank.StraightFlush,
                    HighCard = new Card(CardValue.Ace, Suit.Club),
                    Id = id2,
                }
            };
            var handEvaluator = new Mock<IHandEvaluator>();

            handEvaluator.Setup(h => h.GetHandValues(It.IsAny<List<HandInfo>>())).Returns(handValues);

            var gameEvaluator = new GameEvaluator(handEvaluator.Object);

            // Act
            var results = gameEvaluator.GetBestHand(new List<HandInfo>());

            // Assert
            Assert.Equal(id2, results);
        }

        [Fact]
        public void GameEvaluator_GetBestHand_SameHandRanks_Passes()
        {
            // Arrange
            var id = "testing";
            var id2 = "testing2";
            List<HandValue> handValues = new List<HandValue>()
            {
                new HandValue()
                {
                    HandRank =  HandRank.StraightFlush,
                    HighCard = new Card(CardValue.King, Suit.Club),
                    Id = id,
                },
                new HandValue()
                {
                    HandRank =  HandRank.StraightFlush,
                    HighCard = new Card(CardValue.Ace, Suit.Club),
                    Id = id2,
                }
            };
            var handEvaluator = new Mock<IHandEvaluator>();

            handEvaluator.Setup(h => h.GetHandValues(It.IsAny<List<HandInfo>>())).Returns(handValues);

            var gameEvaluator = new GameEvaluator(handEvaluator.Object);

            // Act
            var results = gameEvaluator.GetBestHand(new List<HandInfo>());

            // Assert
            Assert.Equal(id2, results);
        }

        [Fact]
        public void GameEvaluator_GetBestHand_Tie_Passes()
        {
            // Arrange
            var id = "testing";
            var id2 = "testing2";
            List<HandValue> handValues = new List<HandValue>()
            {
                new HandValue()
                {
                    HandRank =  HandRank.StraightFlush,
                    HighCard = new Card(CardValue.Ace, Suit.Spade),
                    Id = id,
                },
                new HandValue()
                {
                    HandRank =  HandRank.StraightFlush,
                    HighCard = new Card(CardValue.Ace, Suit.Club),
                    Id = id2,
                }
            };
            var handEvaluator = new Mock<IHandEvaluator>();

            handEvaluator.Setup(h => h.GetHandValues(It.IsAny<List<HandInfo>>())).Returns(handValues);

            var gameEvaluator = new GameEvaluator(handEvaluator.Object);

            // Act
            var results = gameEvaluator.GetBestHand(new List<HandInfo>());

            // Assert
            Assert.Contains("tie", results);
        }

        [Fact]
        public void GameEvaluator_IsValidGame_DuplicateIds_Passes()
        {
            // Arrange
            var id = "testing";
            var id2 = "testing2";
            var handInfos = new List<HandInfo>()
            {
                new HandInfo()
                {
                    Cards = new List<string>()
                    {
                        "AC"
                    },
                    Id = id,
                },
                new HandInfo()
                {
                    Cards = new List<string>()
                    {
                        "2C"
                    },
                    Id = id2,
                }

            };
            var gameEvaluator = new GameEvaluator();

            // Act
            var results = gameEvaluator.IsValidGame(handInfos, 2);

            // Assert
            Assert.True(results);
        }

        [Fact]
        public void GameEvaluator_IsValidGame_DuplicateIds_Fails()
        {
            // Arrange
            var id = "testing";
            var handInfos = new List<HandInfo>()
            {
                new HandInfo()
                {
                    Cards = new List<string>()
                    {
                        "AC"
                    },
                    Id = id,
                },
                new HandInfo()
                {
                    Cards = new List<string>()
                    {
                        "2C"
                    },
                    Id = id,
                }

            };
            var gameEvaluator = new GameEvaluator();

            // Act
            var results = gameEvaluator.IsValidGame(handInfos, 2);

            // Assert
            Assert.False(results);
        }

        [Fact]
        public void GameEvaluator_IsValidGame_DuplicateCards_Fails()
        {
            // Arrange
            var id = "testing";
            var handInfos = new List<HandInfo>()
            {
                new HandInfo()
                {
                    Cards = new List<string>()
                    {
                        "AC",
                        "AC",
                        "AC",
                        "3D",
                        "2D",
                    },
                    Id = id,
                },
            };
            var gameEvaluator = new GameEvaluator();

            // Act
            var results = gameEvaluator.IsValidGame(handInfos, 2);

            // Assert
            Assert.False(results);
        }
    }
}
